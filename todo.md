* document Auth0 setup, especially the rules and callback urls
* implement a proper server side API to show an example of JWTs being validated
* investigate capturing client console logs
    * for display on error page
    * for sending to log service
* investigate custom auth0 domain
* add apple sign-in
    * will need a custom Auth0 domain and Apple developer account :(
* add terraform module to create the Auth0 side of things automatically

    
               