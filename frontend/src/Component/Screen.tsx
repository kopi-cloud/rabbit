import makeStyles from "@material-ui/core/styles/makeStyles";
import * as React from "react";
import {
  Paper,
  Theme, useMediaQuery
} from "@material-ui/core";

const maxScreenWidth = 1024;

function mainLayoutBreakpoints(theme: Theme, width: number){
  return {
    [theme.breakpoints.up(width + (theme.spacing(2) * 2))]: {
      width: width,
      marginLeft: 'auto',
      marginRight: 'auto',
    }
  }
}

const useLargeScreenStyle = makeStyles(theme =>({
  mainLayout: {
    width: 'auto',
    ...mainLayoutBreakpoints(theme, maxScreenWidth),
    marginTop: ".5em"
  },
}));

export function LargeScreenContainer(props: {
  children:React.ReactNode,
}) {
  const classes = useLargeScreenStyle();
  return <main className={classes.mainLayout}>
    {props.children}
  </main>
}

function paperBreakpoints(theme: Theme, width: number){
  return {
    [theme.breakpoints.up(width + (theme.spacing(3) * 2))]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    }
  }
}

const useSmallScreenStyle = makeStyles(theme =>({
  mainLayout: {
    width: 'auto',
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    ...mainLayoutBreakpoints(theme, 600),
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    ...paperBreakpoints(theme, 600),
  },
  centerPaper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    ...paperBreakpoints(theme, 600),
    textAlign: "center",
  }
}));

export function SmallScreenContainer(props: {
  children:React.ReactNode,
  center?: boolean,
}) {
  const style = useSmallScreenStyle();
  return <main className={style.mainLayout}>
    <Paper className={props.center ? style.centerPaper : style.paper}>
      {props.children}
    </Paper>
  </main>
}

export const useCardPanelStyle = makeStyles(theme=>({
  header: {
    // reduce the spacing around the header, there was too much whitespace
    padding: "0 1em",
    backgroundColor: 'rgba(0,0,0,.1)',
    // if card has no actions header gets short, minHeight keeps it
    // consistent (AboutScreen has one panel with actions and one without)
    minHeight: "3rem",
},
  headerTitle: {
    // default is h6 and it looks too big to me
    fontSize: "1.5em"
  },
  headerAction: {
    marginTop: 0,
  },
  // same as header, remove extra whitey
  content: {
    padding: "1em",
    // deal with possibility of long date / time strings on mobile
    overflowX: "auto",

  },
  refreshIcon: {
  },
}));

export function useFullScreenDialog():boolean{
  // 960 is the "md" breakpoint, the old withMobileDialog defaulted to "sm",
  // which meant the dialog was fullscreen until view size hit the "next"
  // breakpoint after "sm" (i.e. "md")
  return useMediaQuery('(max-width:960px)', {noSsr: true});
}
