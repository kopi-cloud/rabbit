import React from "react";
import {SmallScreenContainer} from "Component/Screen";
import Typography from "@material-ui/core/Typography";
import {CompactLinearProgress} from "Component/AppButton";

export function SmallScreenSpinner(props: { message: string | React.ReactNode }){
  return <SmallScreenContainer center>
    <Typography paragraph>
      {props.message}
    </Typography>
    <CompactLinearProgress isLoading style={{width: "100%"}}/>
  </SmallScreenContainer>
}