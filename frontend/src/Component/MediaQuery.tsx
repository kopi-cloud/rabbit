import {useMediaQuery} from "@material-ui/core";

export function useReducedMotion():boolean{
  return useMediaQuery('(prefers-reduced-motion: reduce)', {noSsr: true});
}
