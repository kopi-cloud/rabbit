import React from 'react';
import Button, {ButtonProps} from "@material-ui/core/Button";
import {LinearProgress} from "@material-ui/core";

export const primaryButtonProps : ButtonProps = {
  variant:"contained", color:"primary",
  style:{textTransform: "none"},
};

export const PrimaryButton = function(props: ButtonProps){
  return <Button {...primaryButtonProps} {...props}/>
};

export const secondaryButtonProps : ButtonProps = {
  variant:"outlined", color:"primary",
  style:{textTransform: "none"},
};

export const SecondaryButton = function(props: ButtonProps){
  return <Button {...secondaryButtonProps} {...props}/>
};

/** Use this as the child content of the button when you want to show a loading
 * indicator on the button when it's performing its action.
 */
export function ButtonLabel(props: {
  children: React.ReactNode,
  isLoading?: boolean,
  color?: "primary"|"secondary"
}){
  return <span>
    { props.children }
    <CompactLinearProgress isLoading={props.isLoading}
      color={props.color} />
  </span>
}


export function CompactLinearProgress(props: {
  isLoading?: boolean,
  color?: "primary"|"secondary"
  style?: React.CSSProperties,
}){
  const {isLoading} = props;
  const style = {...props.style, height: "2px"};

  /* placeholder that stops the screen content underneath the progress bar
  from "jumping" as indicator is toggled.
  Used to do this by setting the bar to "invisible" but that
  actually caused cpu/gpu load because the animation is running all the time
  even though it's invisible, constantly causing layout events.
   */
  if( !isLoading ){
    return <div style={style} />;
  }

  return <LinearProgress variant="indeterminate"
    color={props.color}
    style={style}
  />
}