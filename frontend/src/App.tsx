import React from 'react';
import './App.css';
import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import {CssBaseline} from "@material-ui/core";
import {AuthenticationProvider} from 'Auth/AuthenticationProvider';
import {ApiProvider} from "Auth/ApiProvider";
import {AuthorizationProvider} from "Auth/AuthorizationProvider";
import {AppNavBar} from "Navigation/AppNavBar";
import {ReactErrorBoundary} from "Error/ReactErrorBoundary";
import {ErrorDialogProvider} from "Error/ErrorDialog";
import {HomeScreen} from "Screen/HomeScreen";
import {HelloWorldScreen} from "Screen/HelloWorldScreen";
import {ServerErrorScreen} from "Screen/ServerErrorScreen";
import {IndexScreen} from 'Screen/Anonymous/IndexScreen';
import {NavigationProvider} from "Navigation/NavigationProvider";
import {WelcomeScreen} from "Screen/Anonymous/WelcomeScreen";
import {AnonScreen1} from "Screen/Anonymous/AnonymousScreen1";
import {AnonScreen2} from "Screen/Anonymous/AnonymousScreen2";

export const App: React.FC = () => {
  return <MuiThemeProvider theme={theme}>
    <CssBaseline/>
    <ReactErrorBoundary>
      <ErrorDialogProvider>
        <NavigationProvider>
          <IndexScreen/>
          <WelcomeScreen/>
          <AnonScreen1/>
          <AnonScreen2/>
          <AuthenticationProvider>
            <ApiProvider>
              <AuthorizationProvider>
                <AppNavBar/>
                <HomeScreen/>
                <HelloWorldScreen/>
                <ServerErrorScreen/>
              </AuthorizationProvider>
            </ApiProvider>
          </AuthenticationProvider>
        </NavigationProvider>
      </ErrorDialogProvider>
    </ReactErrorBoundary>
  </MuiThemeProvider>;
};

export const theme = createMuiTheme({
  overrides: {
    MuiAppBar: {
      colorPrimary: {
        backgroundColor: "#343a40",
      },
    },
  }
});
