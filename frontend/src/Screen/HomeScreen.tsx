import React from 'react';
import {SmallScreenContainer} from "Component/Screen";
import {Typography} from "@material-ui/core";
import {NavTransition} from "Navigation/NavigationProvider";

const homeUrl = "/home";

// const log = console;

export function getHomeScreenLink(): string{
  return homeUrl;
}

export function isHomeScreenPath(path: string): boolean{
  if( !path ){
    return false;
  }
  return path.toLowerCase().startsWith(homeUrl);
}

export function HomeScreen(){
  return <NavTransition isPath={isHomeScreenPath} title={"Rabbit - home"}>
    <SmallScreenContainer>
      <Typography>Home screen</Typography>
    </SmallScreenContainer>
  </NavTransition>
}

