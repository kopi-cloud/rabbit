import React from 'react';
import {Button, Typography} from "@material-ui/core";
import {ButtonLabel, primaryButtonProps} from "Component/AppButton";
import {TextSpan} from "Component/TextSpan";
import {SmallScreenContainer} from "Component/Screen";
import {CompactErrorPanel} from "Error/CompactErrorPanel";
import {useRabbitApi} from "Auth/ApiProvider";
import {ErrorInfo} from "Error/ErrorUtil";
import {NavTransition} from "Navigation/NavigationProvider";

const log = console;

const helloWorldUrl = "/helloworld";

export function getHelloWorldScreenLink():string{
  return helloWorldUrl;
}

export function isHellowWorldScreenPath(path:string){
  return path.toLowerCase().startsWith(helloWorldUrl);
}

export function HelloWorldScreen(){
  return <NavTransition isPath={isHellowWorldScreenPath} title={"Rabbit - Hello world"}>
    <SmallScreenContainer>
      <Typography paragraph>Hello, world.</Typography>
      <ServerErrorExample/>
      <BadReactComponentExample/>
    </SmallScreenContainer>
  </NavTransition>
}

function BadReactComponentExample(){
  const [showBadReactComponent, setShowBadReactComponent] =
    React.useState(false);

  return <Typography paragraph>
    <Button {...primaryButtonProps} onClick={() => {
      setShowBadReactComponent(!showBadReactComponent);
    }}>
      Toggle React error
    </Button>
    { showBadReactComponent &&
      <TextSpan>
        {generateIntentionalError()}
      </TextSpan>
    }
  </Typography>
}

function ServerErrorExample(){
  const [isCallingServer, setIsCallingServer] =
    React.useState(false);
  const [serverError, setServerError] =
    React.useState(undefined as undefined|ErrorInfo);
  const rabbitApi = useRabbitApi();

  async function doServerCall(){
    log.debug("doServerCall() called");
    setIsCallingServer(true);
    try {
      const userInfo = await rabbitApi.findCurrentUserInfo();
      log.debug("userInfo", userInfo);
      // after a real call to the server, you'd be checking the result and/or
      // storing the result data
      // this serves to "clear" the error as if the most recent button press
      // "succeeded"
      setServerError(undefined);
    }
    catch( error ){
      log.error("doServerCall() error", error);
      setServerError({message: "server call failed", problem: error});
    }
    finally {
      setIsCallingServer(false);
    }
  }

  return <Typography paragraph>
    <Button {...primaryButtonProps} disabled={isCallingServer}
      onClick={doServerCall}
    >
      <ButtonLabel isLoading={isCallingServer}>
        Do server call
      </ButtonLabel>
    </Button>
    &nbsp;
    <CompactErrorPanel error={serverError}/>
  </Typography>

}


function generateIntentionalError(): string{
  if( "intentional error".length > 0 ){
    throw Error("intentionally generated error");
  }
  return "";
}

