import React from 'react';
import {SmallScreenContainer} from "Component/Screen";
import {Typography} from "@material-ui/core";
import {NavTransition, useNavigation} from "Navigation/NavigationProvider";
import {PrimaryButton} from "Component/AppButton";
import {getAnonScreen1Link} from "Screen/Anonymous/AnonymousScreen1";

const anon2Url = "/anon2";

export function getAnonScreen2Link():string{
  return anon2Url;
}

export function isAnonScreen2Path(path:string){
  return path.toLowerCase().startsWith(anon2Url);
}

export function AnonScreen2(){
  const nav = useNavigation();
  return <NavTransition isPath={isAnonScreen2Path} title="Rabbit - Screen 2">
    <SmallScreenContainer center>
      <Typography>Screen 2</Typography>
      <br/>
      <Typography paragraph>
        Here's a button that navigates to Screen 1.
      </Typography>
      <PrimaryButton href={getAnonScreen1Link()}
        onClick={e=>nav.navigateTo(e, getAnonScreen1Link())}
      >
        Screen 1
      </PrimaryButton>
    </SmallScreenContainer>
  </NavTransition>
}