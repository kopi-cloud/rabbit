import React, {SyntheticEvent} from 'react';
import {SmallScreenContainer} from "Component/Screen";
import {Button, Typography} from "@material-ui/core";
import {ButtonLabel, primaryButtonProps} from "Component/AppButton";
import {getHomeScreenLink} from "Screen/HomeScreen";
import Divider from "@material-ui/core/Divider";
import Link from "@material-ui/core/Link";
import {NavTransition, useNavigation} from "Navigation/NavigationProvider";
import {getAnonScreen1Link} from "Screen/Anonymous/AnonymousScreen1";
import {Config} from "Config";

const welcomeUrl = Config.authnLogoutUrl;

export function getWelcomeScreenLink():string{
  return welcomeUrl;
}

export function isWelcomeScreenPath(path:string){
  const normalizedPath = path.toLowerCase();
  return normalizedPath.startsWith(welcomeUrl);
}

export function WelcomeScreen(){
  return <NavTransition isPath={isWelcomeScreenPath} title={"Rabbit - Welcome"}>
    <>
      <WelcomeContainer/>
      <NavigationExampleLinkContainer/>
    </>
  </NavTransition>;
}

function WelcomeContainer(){
  const [loginWasClicked, setLoginWasClicked] = React.useState(false);
  return <SmallScreenContainer center>
    <Typography paragraph variant="h5">Welcome to the Rabbit app!</Typography>
    <Divider variant={"middle"}/>
    <br/>
    <Typography paragraph variant="body1">
      Rabbit doesn't do anything. It's just a demo of a React app
      that integrates{" "}
      <Link target="_blank" rel="noopener"
        href="https://material-ui.com/"
      >
        Material-UI
      </Link>{" "}
      and the{" "}
      <Link target="_blank" rel="noopener"
        href="https://auth0.com/docs/libraries/auth0-spa-js"
      >
        Auth0 SPA
      </Link>{" "}
      library.
      Rabbit is MIT licensed, scope it out on{" "}
      <Link target="_blank" rel="noopener"
        href="https://bitbucket.org/kopi-cloud/rabbit.git"
      >
        Bitbucket
      </Link>
    </Typography>
    <Typography paragraph variant="body1">
      In order to use the app, you'll need to login with an account
      (use Twitter / Google or sign up with an email).
      Rabbit is not trying to harvest your details and you won't be sent any
      spam (you will get a verification mail if you sign up via email).
      It's just that one of the things Rabbit is trying to demonstrate is
      verified account enforcement via Auth0.
    </Typography>
    <Typography variant="body1">
      If signing up to random websites with your email is something that
      concerns you - consider using an{" "}
      <Link target="_blank" rel="noopener" href="https://kopi.cloud">
        email forwarder service
      </Link>{" "}
      to protect your email 😊
      <br/>
      (No, Rabbit is not just content marketing for Kopi - but hey,
      two birds...)
    </Typography>
    <br/>
    <Button {...primaryButtonProps} href={getHomeScreenLink()}
      onClick={() => setLoginWasClicked(true)}
    >
      <ButtonLabel isLoading={loginWasClicked}>
        Log in
      </ButtonLabel>
    </Button>
  </SmallScreenContainer>
}

function NavigationExampleLinkContainer(){
  const nav = useNavigation();
  return <SmallScreenContainer center>
    <Typography paragraph variant="body1">
      <Link href={getAnonScreen1Link()} onClick={(event: SyntheticEvent) => {
        nav.navigateTo(event, getAnonScreen1Link())
      }}>
        Anonymous navigation example
      </Link>
    </Typography>
  </SmallScreenContainer>
}