import React, {SyntheticEvent} from 'react';
import {SmallScreenContainer} from "Component/Screen";
import {Link, Typography} from "@material-ui/core";
import {NavTransition, useNavigation} from "Navigation/NavigationProvider";
import {getAnonScreen2Link} from "Screen/Anonymous/AnonymousScreen2";
import {getWelcomeScreenLink} from "Screen/Anonymous/WelcomeScreen";
import {useReducedMotion} from "Component/MediaQuery";

const anon1Url = "/anon1";

export function getAnonScreen1Link():string{
  return anon1Url;
}

export function isAnonScreen1Path(path:string){
  return path.toLowerCase().startsWith(anon1Url);
}

export function AnonScreen1(){
  const nav = useNavigation();
  const reducedMotion = useReducedMotion();
  return <NavTransition isPath={isAnonScreen1Path} title="Rabbit - Screen 1">
    <SmallScreenContainer center>
      <Typography>Screen 1</Typography>
      <br/>
      <Typography paragraph>
        Here's a link to{" "}
        <Link href={getAnonScreen2Link()} onClick={(e: SyntheticEvent)=>
          nav.navigateTo(e, getAnonScreen2Link())
        }>
          Screen 2
        </Link>
      </Typography>
      <Typography paragraph>
        Or go back to the{" "}
        <Link href={getWelcomeScreenLink()} onClick={
          (e: SyntheticEvent)=> nav.navigateTo(e, getWelcomeScreenLink())
        }>
          Welcome screen
        </Link>
        .
      </Typography>
      <Typography paragraph>
        Reduced motion: {reducedMotion.toString()}
      </Typography>
    </SmallScreenContainer>
  </NavTransition>
}