import {isWelcomeScreenPath} from "Screen/Anonymous/WelcomeScreen";
import {isAnonScreen1Path} from "Screen/Anonymous/AnonymousScreen1";
import {isAnonScreen2Path} from "Screen/Anonymous/AnonymousScreen2";
import {isIndexPath} from "Screen/Anonymous/IndexScreen";

export function isAnonScreenPath(path: string){
  return isIndexPath(path) || isWelcomeScreenPath(path) ||
    isAnonScreen1Path(path) || isAnonScreen2Path(path);
}

