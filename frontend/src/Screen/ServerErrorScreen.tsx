import React from 'react';
import {Button, Typography} from "@material-ui/core";
import {ButtonLabel, primaryButtonProps} from "Component/AppButton";
import {SmallScreenContainer} from "Component/Screen";
import {CompactErrorPanel} from "Error/CompactErrorPanel";
import {useRabbitApi} from "Auth/ApiProvider";
import {ErrorInfo} from "Error/ErrorUtil";
import {useOpenErrorDialog} from "Error/ErrorDialog";
import {NavTransition} from "Navigation/NavigationProvider";

const log = console;

const serverErrorScreenUrl = "/server-error";

export function getServerErrorScreenLink():string{
  return serverErrorScreenUrl;
}

export function isServerErrorScreenPath(path:string){
  return path.toLowerCase().startsWith(serverErrorScreenUrl);
}

export function ServerErrorScreen(){
  return <NavTransition isPath={isServerErrorScreenPath}
    title={"Rabbit - Server error"}
  >
    <SmallScreenContainer>
      <Typography paragraph>
        This call intentionally throws an error.
      </Typography>
      <ServerErrorExample/>
    </SmallScreenContainer>
  </NavTransition>
}

const fastDelay = 300;
const slowDelay = 3000 + fastDelay;

function ServerErrorExample(){
  const [isCallingServer, setIsCallingServer] =
    React.useState(false);
  const [serverError, setServerError] =
    React.useState(undefined as undefined|ErrorInfo);
  const rabbitApi = useRabbitApi();
  const handleError = useOpenErrorDialog();

  /** To see the error dialog handling a background error while a foreground
   * error is being handled:
   * - click the button then immediately click the error
   *   link so that the error dialog is being shown for the "fast error"
   * - do nothing and wait for the the slowDelay timeout
   * - you will see the error dialog shows error messages for both the
   *   slow error and the fast error.
   */
  async function doServerCall(){
    log.debug("doServerCall() called");
    setIsCallingServer(true);
    const slowPromise = rabbitApi.badServerCall("slow call", slowDelay);
    try {
      const fastResult = await rabbitApi.badServerCall("fast call", fastDelay);
      // after a real call to the server, you'd be checking the result and/or
      // storing the result data
      console.log("fast result", {slowResult: fastResult});
      // this serves to "clear" the error as if the most recent button press
      // "succeeded"
      setServerError(undefined);
    }
    catch( error ){
      log.error("doServerCall() fast error", error);
      setServerError({message: "server call failed", problem: error});
    }
    finally {
      setIsCallingServer(false);
    }

    try {
      const slowResult = await slowPromise;
      console.log("slow result", {slowResult: slowResult});
    }
    catch( error ){
      log.error("doServerCall() slow error", error);
      handleError({
        type: "handleError",
        error: {message: "slow result error", problem: error} });
    }
  }

  return <Typography paragraph>
    <Button {...primaryButtonProps} disabled={isCallingServer}
      onClick={doServerCall}
    >
      <ButtonLabel isLoading={isCallingServer}>
        Do server call
      </ButtonLabel>
    </Button>
    &nbsp;
    <CompactErrorPanel error={serverError}/>
  </Typography>

}
