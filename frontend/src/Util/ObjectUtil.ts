
export function safeStringify(o: any){
  if( o === undefined ){
    return "[undefined]";
  }

  if( o instanceof HTMLElement ){
    return "HTMLElement";
  }

  if( o.hasOwnProperty('type') && o.type === "JSXElement" ){
    return "JSXElement";
  }

  return JSON.stringify(o);
}

export function delay(ms: number):Promise<never> {
  return new Promise(resolve => setTimeout(resolve, ms));
}

