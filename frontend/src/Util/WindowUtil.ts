
/** the location of the window without paths, params, etc.
 i.e. given window location "http://localhost:8080/blah?wibble=wobble#flobble"
 function returns "http://localhost:8080"
 */
export function serverLocationUrl(){
  return window.location.protocol + "//" + window.location.host
}
