import * as React from "react";
import {
  AppBar,
  Hidden,
  IconButton,
  makeStyles,
  Menu,
  MenuItem,
  SvgIcon,
  Toolbar,
  Typography
} from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import MenuIcon from "@material-ui/icons/Menu";
import {AppDrawer} from "Navigation/AppDrawer";
import {expiry, issuedAt, useAuthn} from "Auth/AuthenticationProvider";
import {ReactComponent as RabbitSvg} from 'Svg/Rabbit.svg';
import {useAuthz} from "Auth/AuthorizationProvider";
import {getHomeScreenLink} from "Screen/HomeScreen";
import {useNavigation} from "Navigation/NavigationProvider";
import {getHelloWorldScreenLink} from "Screen/HelloWorldScreen";

const log = console;
let jwtDecode = require('jwt-decode');

const useAppBarStyle = makeStyles({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
});

export function AppNavBar(){
  const nav = useNavigation();
  const style = useAppBarStyle();
  const [drawerOpen, setDrawerOpen] = React.useState(false);

  return <AppBar position="static">
    <Toolbar variant={"dense"}>
      <IconButton color="inherit" href={getHomeScreenLink()}
        onClick={event=>nav.navigateTo(event, getHomeScreenLink())}
      >
        <HomeIcon />
      </IconButton>

      <MenuShortcutBar>
        <MenuShortcutItem
          href={getHelloWorldScreenLink()}
        >
          Hello
        </MenuShortcutItem>
      </MenuShortcutBar>

      {/*This pushes the account icon over to the right*/}
      <Typography variant="h6" color="inherit" className={style.grow}/>
      <div>
        {/*<IconButton color="inherit" onClick={openFeedbackDialog}>*/}
        {/*  <FeedbackIcon fontSize="large"/>*/}
        {/*</IconButton>*/}
        <AccountMenu/>
        <IconButton className={style.menuButton} color="inherit"
          onClick={()=>setDrawerOpen(true)}
        >
          <MenuIcon/>
        </IconButton>
        <AppDrawer anchor={"right"}
          open={drawerOpen}
          toggleDrawer={setDrawerOpen} />
      </div>
    </Toolbar>
  </AppBar>
}

function MenuShortcutBar(props:{children: React.ReactNode}){
  return <Hidden smDown>
    {/* Avoid shortcuts wrapping which causes AppBar to grow in height */}
    <span style={{
      display: "flex", flexWrap: "nowrap", overflow: "hidden"
    }}>
      {props.children}
    </span>
  </Hidden>
}

function MenuShortcutItem(props: {
  children: React.ReactNode;
  href: string,
}){
  const nav = useNavigation();
  const style = useAppBarStyle();
  return <IconButton className={style.menuButton} color="inherit"
    href={props.href}
    onClick={event=>nav.navigateTo(event, props.href)}
  >
    {props.children}
  </IconButton>
}

function AccountMenu(){
  const {identity, logout, claim, getToken} = useAuthn();
  const currentUser = useAuthz();
  const[ menuIsOpen, setMenuIsOpen] = React.useState(false);
  const menuAnchorRef = React.useRef<HTMLButtonElement>(null!);

  function closeMenu(){
    setMenuIsOpen(false);
  }

  return <>
    <IconButton ref={menuAnchorRef}
      onClick={()=> setMenuIsOpen(true)}
      style={{paddingRight: 20}} color="inherit"
    >
      <SvgIcon><RabbitSvg/></SvgIcon>
    </IconButton>

    <Menu id="menu-appbar"
      anchorEl={menuAnchorRef.current}
      anchorOrigin={{vertical: 'top', horizontal: 'right'}}
      transformOrigin={{vertical: 'top', horizontal: 'right'}}
      open={menuIsOpen}
      onClose={()=> setMenuIsOpen(false)}
    >
      <MenuItem onClick={()=>{
        log.debug("authn identity and claims", identity, claim);
        closeMenu();
      }}>
        <Typography>You are {identity.email}</Typography>
      </MenuItem>
      <MenuItem onClick={()=>{
        closeMenu();
      }}>
        <Typography>Your roles: {currentUser.roles.join(",")}</Typography>
      </MenuItem>
      <MenuItem onClick={async ()=>{
        log.debug("Logged in at clicked");
        const decodedJwt: any = jwtDecode(await getToken());
        log.debug("authn token", decodedJwt);
        log.debug("issued at", decodedJwt.iat, issuedAt(decodedJwt));
        log.debug("expires at", decodedJwt.exp, expiry(decodedJwt));
        closeMenu();
      }}>
        <Typography>
          Logged in at {issuedAt(claim).toLocaleTimeString()}
        </Typography>
      </MenuItem>
      <MenuItem onClick={()=>{
        log.debug("clicked logout");
        logout();
      }}>
        <Typography>Log out</Typography>
      </MenuItem>
    </Menu>
  </>
}

