import * as React from "react";
import {makeStyles} from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import {ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import {getHomeScreenLink, isHomeScreenPath} from "Screen/HomeScreen";
import {
  getHelloWorldScreenLink,
  isHellowWorldScreenPath
} from "Screen/HelloWorldScreen";
import {
  getServerErrorScreenLink,
  isServerErrorScreenPath
} from "Screen/ServerErrorScreen";
import {useNavigation} from "Navigation/NavigationProvider";
import {getAnonScreen1Link, isAnonScreen1Path} from "Screen/Anonymous/AnonymousScreen1";

const useAppDrawerStyle = makeStyles({
  list: {
    // hardcoded because we often need to be reminded that mobile is a thing
    width: 250,
  },
});


export function AppDrawer(props: {
  anchor: 'left' |'right',
  open: boolean,
  toggleDrawer: (open:boolean)=>void,
}){
  const style = useAppDrawerStyle();

  // const openFeedbackDialog = useOpenFeedbackDialog();

  const sideList = (
    <div className={style.list}>
      <List>
        <ListButton href={getHomeScreenLink()}
          isCurrent={isHomeScreenPath}
          icon={<HomeIcon/>} description={"Home"} />
        <ListButton href={getHelloWorldScreenLink()}
          isCurrent={isHellowWorldScreenPath}
          description="Hello world" />
        <ListButton href={getServerErrorScreenLink()}
          isCurrent={isServerErrorScreenPath}
          description="Server error" />
        <ListButton href={getAnonScreen1Link()}
          isCurrent={isAnonScreen1Path}
          description="Anon Screen 1" />
      </List>
    </div>
  );

  // const iOS = /iPad|iPhone|iPod/.test(navigator.userAgent);

  const onClose = ()=> props.toggleDrawer(false);
  return <SwipeableDrawer
    // disableBackdropTransition={!iOS}
    // disableDiscovery={iOS}
    open={props.open}
    onClose={onClose}
    onOpen={()=> props.toggleDrawer(true)}
    anchor={props.anchor}
  >
    <div
      tabIndex={0}
      role="button"
      onClick={onClose}
      onKeyDown={onClose}
    >
      {sideList}
    </div>
  </SwipeableDrawer>;
}

function ListButton(props: {
  href: string,
  isCurrent: (path: string)=>boolean,
  // feature?: FeatureEnumV1,
  description: string,
  icon?: JSX.Element,
  adminOnly?: boolean
}){
  const nav = useNavigation();
  // const authz = useAuthZ();

  // if( props.feature && !AuthZ.hasFeature(authz.currentUser, props.feature) ){
  //   return null;
  // }

  // if needs admin but user isn't
  // if( props.adminOnly && !AuthZ.isAdmin(authz.currentUser) ){
  //   return null;
  // }

  const isCurrent = props.isCurrent(nav.currentLocation);
  const description = <span style={{fontWeight: isCurrent ? "bold":"normal"}}>
    {props.description}
  </span>;

  return <ListItem button href={props.href}
    onClick={event=>nav.navigateTo(event, props.href)}
  >
    { props.icon &&
      <ListItemIcon>
        {props.icon}
      </ListItemIcon>
    }
    <ListItemText primary={description} />
  </ListItem>;
}
