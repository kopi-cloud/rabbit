import React, {SyntheticEvent, useContext, useState} from "react";
import {useLocation} from "Navigation/UseLocation";
import Fade from "@material-ui/core/Fade";

// this comes from eyeballing the drawer css in my browser, maybe there's
// a better place in the MUI API to get it from (theme defaults or something?)
const muiAppDrawerTimeout = 225;

/** The important part about this is that it controls exactly when the
 * pushState happens, this gives the AppDrawer (or any other temp stuff like
 * dialogs) time to transition away before ios "snapshots" the screen
 * (iOS then uses that snapshot when navigating "back" to the screen).
 * <p/>
 * Defined as 2 x appDrawer for no good reason other than I like the look of it.
 * This is very likely "developer bias", where I think it looks right because
 * I wrote it and want to watch it happen.
 * <p/>
 * Should probably be appDrawerTimeout or less. Good design, especially as it
 * relates to animations should be subliminal / barely noticable. My
 * personal desire that everyone should notice the cool screentransitions is
 * the antithesis of good design.
 */
export const navTime = muiAppDrawerTimeout * 2;

// const log = console;

export interface NavigationState {
  currentLocation: string,
  isNavigatingTo: string | undefined,
  navigateTo: (event: SyntheticEvent, to: string) => void,
}

const NavigationContext = React.createContext({} as NavigationState );
export const useNavigation = ()=> useContext(NavigationContext);

export function NavigationProvider(props: {children: React.ReactNode}){
  const location = useLocation();
  const [navigatingTo, setNavigatingTo] = useState(undefined as string | undefined);

  const navTo = React.useCallback((event, to)=> {
    event.preventDefault();
    setNavigatingTo(to);
    setTimeout(()=>{
      location.pushState(to);
      setNavigatingTo(undefined);
    }, navTime);
  }, [location]);

  return <NavigationContext.Provider value={{
    currentLocation: location.currentLocation,
    isNavigatingTo: navigatingTo,
    navigateTo:navTo,
  }}>
    {props.children}
  </NavigationContext.Provider>;
}

export function NavTransition(props: {
  isPath: (path: string)=>boolean,
  title: string,
  children: React.ReactNode
}){
  const nav = useNavigation();
  const {title, isPath} = props;
  let isOn = isPath(nav.currentLocation);
  /* it's important to know which "direction" the navigation is going; so that
  we can transition "out of" or "away from" the "old" screen, while
  simultaneously transitioning "in" or "to" the "new" screen. */
  let isNavToThis = nav.isNavigatingTo && isPath(nav.isNavigatingTo);
  let isNavAway = nav.isNavigatingTo && !isNavToThis;

  if( isOn ){
    window.document.title = title;
  }

  /* div inside fade is necessary if using Slide transition */
  return <Fade
    timeout={navTime}
    in={!isNavAway && (isNavToThis || isOn)}
    /* Each NavTransition is intended to occupy the same position on screen;
    without this, the "from" and "to" screen will display next to each other
    instead of in the same place.  It may be necessary to set "relative"
    somewhere in your element hierarchy if using this code outside of rabbit.*/
    style={{position: "absolute", width: "100%",}}
  >
    <div>
      { (isNavToThis || isOn) &&
        props.children
      }
    </div>
  </Fade>;
}