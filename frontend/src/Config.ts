/** Environment specific config stuff, set the environment at build time by
 * specifying REACT_APP_RABBIT_ENV.
 *
 * Note that the environment specific stuff is overkill right now (everything
 * is the same between environments) - but it'll be needed later.
 */

const defaultEnv = {
  // client config
  /** From Auth0: /Applications/Rabbit/Settings/Domain */
  auth0Domain: "rabbit-netlify.auth0.com",
  /** From Auth0: /Applications/Rabbit/Settings/Client ID */
  auth0ClientId: "VdQLXSPvs4hsNt3xFDwTuyunjC5X1sz0",
  /** From Auth0: /APIs/Rabbit API/Settings/Identifier */
  authnApiAudience: "https://rabbit-api",

  /** This must be mirrored in the Auth0 setting
   * `/Applications/Rabbit/Settings/Allowed callback URLs`
   * with a value like:
   * "http://localhost:3000/authn, https://rabbit.netlify.app/authn
   * The localhost url for local devlopment, the netlify url for deployment
   * on netlify.
   */
  authnCallbackUrl: "/authn",
  /** This must be mirrored in the Auth0 setting
   * `/Applications/Rabbit/Settings/Allowed logout URLs`
   * with a value like:
   * "http://localhost:3000/welcome, https://rabbit.netlify.app/welcome".
   */
  authnLogoutUrl: "/welcome",

  /** As well as the above two settings, the setting in Auth0
   * `/Applications/Rabbit/Settings/Allowed web origins`
   * must be a value like:
   * "http://localhost:3000, https://rabbit.netlify.app"
   */

  // "server" config
  /** From Auth0: /Rules/add-email-to-access-tokenrule */
  rabbitApiEmailProp: "http://rabbit_email",
  /** From Auth0: /Rules/add-email-to-access-tokenrule */
  rabbitApiEmailVerProp: "http://rabbit_email_verified",
};


type EnvironmentName = "prd"|"local";

export interface EnvironmentConfig {
  environmentName: EnvironmentName,
  isProd: boolean,
}

const localConfig:EnvironmentConfig = {
  environmentName: "local",
  isProd: false,
};

const prdConfig:EnvironmentConfig = {
  environmentName: "prd",
  isProd: true,
};

function chooseEnv(env:string|undefined){
  if( env === 'prd' ){
    return prdConfig
  }
  else {
    return localConfig;
  }
}

export const Config = {
  ...defaultEnv, ...chooseEnv(process.env.REACT_APP_RABBIT_ENV),
};
