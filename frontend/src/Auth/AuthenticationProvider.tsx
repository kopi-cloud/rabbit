import React, {ReactNode, useContext, useEffect, useState} from "react";
import createAuth0Client from "@auth0/auth0-spa-js";
import Auth0Client from "@auth0/auth0-spa-js/dist/typings/Auth0Client";
import {SmallScreenSpinner} from "Component/SmallScreenSpinner";
import {UnverifiedEmailContainer} from "Auth/UnverifiedEmailContainer";
import {useOpenErrorDialog} from "Error/ErrorDialog";
import {useLocation} from "Navigation/UseLocation";
import {getWelcomeScreenLink} from "Screen/Anonymous/WelcomeScreen";
import {serverLocationUrl} from "Util/WindowUtil";
import {isAnonScreenPath} from "Screen/Anonymous/AnonymousScreens";
import {getAuthnCallbackUrl, removeAuth0StateFromUrl} from "Auth/Auth0Util";
import {Config} from "Config";

const log = console;

/* This default context value is purposefully invalid; if client-code ever uses
this, it means that they've consumed the context with no Authentication
provider. */
const AuthnContext = React.createContext({
  isAuthenticated: false,
  isLoading: false,
  identity: {email: "no-context-provider-set"} as AuthnIdentity,
  claim: {email: "no-context-provider-set"} as AuthnClaim,
  logout: throwNoProviderError,
  getToken: throwNoProviderError,
} as AuthnContextInfo);
export const useAuthn = () => useContext(AuthnContext);

/* This exists to separate the "anonymous screen" logic from
the actual authn stuff.  It just simplifies the overall authn logic a bit,
anything that reduces that 150 line monster is a good thing in my book. */
export function AuthenticationProvider(props: {
  children: ReactNode,
}){
  const {currentLocation} = useLocation();

  if( isAnonScreenPath(currentLocation) ){
    log.debug("not authenticated - on an unauth path", {currentLocation});
    return null;
  }

  return <AuthenticationContextProvider>
    {props.children}
  </AuthenticationContextProvider>
}

function AuthenticationContextProvider(props: {
  children: ReactNode,
}){
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isAutoLogging, setIsAutoLogging] = React.useState(false);
  const [isLoggingOut, setIsLoggingOut] = React.useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [auth0Identity, setAuth0Identity] =
    useState(undefined as undefined|AuthnIdentity);
  const [auth0Claim, setAuth0Claim] =
    useState(undefined as undefined|AuthnClaim);
  const [auth0Client, setAuth0Client] = useState(
    undefined as undefined|Auth0Client );
  const errorHandler = useOpenErrorDialog();

  useEffect(() => {
    async function initAuth0(){
      const redirectUri = window.location.origin;
      log.debug("creating auth0 client", {redirectUri, Config});
      const auth0FromHook = await createAuth0Client({
        domain: Config.auth0Domain,
        client_id: Config.auth0ClientId,
        audience: Config.authnApiAudience,
        redirect_uri: getAuthnCallbackUrl(), });
      log.debug("auth0 client created");
      /* Remember, setting React state is async - we're setting it so a *future*
      renders can see the value.  The `auth0Client` reference in *this* render
      was already bound to undefined, so we must use the `auth0FromHook`
      reference. */
      setAuth0Client(auth0FromHook);

      await removeAuth0StateFromUrl(auth0FromHook, errorHandler);

      const isAuthenticated = await auth0FromHook.isAuthenticated();
      setIsAuthenticated(isAuthenticated);

      if( isAuthenticated ){
        log.debug("retrieving auth0 details");
        setAuth0Identity(await auth0FromHook.getUser());
        setAuth0Claim(await auth0FromHook.getIdTokenClaims() as AuthnClaim);
      }
      else {
        log.debug("not authenticated - initiating auto-login");
        setIsAutoLogging(true);
        /* This is where we redirect the user to login, they might
        have to go through multiple steps during the login the login process
        (auth0 login page, google authorization page, etc.) */
        auth0FromHook.loginWithRedirect({
          /* We set targetUrl so that, once the user is finally authenticated,
          we eventually show them the screen they were originally trying to
          get to */
          appState: {targetUrl: window.location.pathname}
        }).catch((e) => {
          errorHandler({type: "handleError", error: e});
        });
      }

      setIsLoading(false);
    }

    initAuth0().catch(e=>errorHandler({
      type: "handleError",
      error: {message: "while calling initAuth0", problem: e } }));

    /* c-r-a EsLint wants me to put errorHandler in the useEffect() deps,
     but that causes a loop because errorHandler changes as invoking the
     handler adds to the list of errors.
     Maybe our ErrorHandling is wrong to work that way?
     Can we shield the context from changing by pusing the list or errors
     down into a contained component or something? Or do the dodgy and change
     it from being proper react state to being a global variable? Gross.
     eslint-disable-next-line react-hooks/exhaustive-deps */
  }, []);

  /* these have to be React callbacks, or the authn context will change every
  render, which causes things that depend on the context to re-render */
  const onLogout = React.useCallback(()=>{
    log.debug("AuthnContext logout() called");
    setAuth0Identity(undefined);
    setAuth0Claim(undefined);
    setIsAuthenticated(false);
    setIsLoggingOut(true);

    if( auth0Client ){
      auth0Client.logout({
        client_id: Config.auth0ClientId,
        /* `returnTo` URL must be registered with Auth0 as an
        "allowed logout url". We prepend serverLocationUrl because Auth0
        requires that we pass an absolute location. */
        returnTo: serverLocationUrl() + getWelcomeScreenLink() });
    }
    else {
      log.debug("AuthnContext logout() called when no auth0Client set");
    }
  }, [auth0Client]);

  const onGetToken = React.useCallback(()=> {
    if( !auth0Client ){
      log.debug("AuthnContext onLoginWithRedirect called with no auth0Client");
      return;
    }
    return auth0Client.getTokenSilently(undefined);
  }, [auth0Client]);

  if( isLoggingOut ){
    log.debug("show spinner because logging out");
    return <SmallScreenSpinner message={"Logging out"}/>;
  }

  if( !auth0Client ){
    log.debug("show spinner because creating Auth0 client");
    return <SmallScreenSpinner message={"Loading Auth0 client"}/>;
  }

  // isLoading is a misnomer, it will be true while "authenticating" too
  if( isLoading ){
    log.debug("show spinner because isLoading");
    return <SmallScreenSpinner message={"Authenticating"}/>;
  }

  if( isAutoLogging ){
    log.debug("show spinner because auto logging in");
    return <SmallScreenSpinner message={"Redirecting browser to Auth0"}/>;
  }

  if( !isAuthenticated ){
    log.debug("show login screen because not authenticated ");
    return <SmallScreenSpinner message={"Authenticating"}/>;
  }
  else if( !auth0Identity || !auth0Claim ){
    log.debug("show spinner because authn details still loading");
    return <SmallScreenSpinner message={"Authenticating"}/>;
  }

  if( !auth0Identity.email ){
    /* this would indicate a problem with the Auth0 setup,
    should probably add a screen to catch this */
    log.debug("no email claim", auth0Identity);
  }

  if( !auth0Identity.email_verified ){
    // twitter will only send back email if it's been verified
    if( !auth0Identity.sub.startsWith("twitter") ){
      log.debug("unverified email", auth0Identity);
      return <UnverifiedEmailContainer
        auth0Client={auth0Client} identity={auth0Identity} />;
    }
  }

  /* Note the lack of "await", meaning this is done in the background.
   Won't really benefit though, since we'll usually be waiting for it
   straight away to do AuthZ, but oh well. */
  auth0Client.getTokenSilently().catch(e=>errorHandler({
    type: "handleError",
    error: {message: "while getting initial token", problem: e} }));

  return <AuthnContext.Provider value={{
    isAuthenticated,
    identity:auth0Identity,
    claim: auth0Claim,
    isLoading,
    getToken: onGetToken as GetTokenFunction,
    logout: onLogout,
  }}>{props.children}</AuthnContext.Provider>
}

/**
 * The members of this are modelled just by me having logged in with Google,
 * need to experiment, read doco and validate what exactly is guaranteed here.
 * Need to mark things optional if they're not guaranteed.
 * If something is optional depending on provider but you want to make it
 * a hard dependency of your app, then you need to add conditions to the
 * render method to enforce behaviour for when they're missing so that
 * consumers of the context aren't having to deal with that optionality.
 */
export interface AuthnIdentity {
  email: string,
  email_verified: boolean,
  updated_at: string,
  sub: string,

  nickname?: string,
  name?: string,
}

export interface AuthnClaim {
  readonly email: string,
  readonly email_verified: boolean,

  readonly aud: string;
  readonly iss: string;

  readonly iat: number;
  readonly exp: number,
  readonly updated_at: string,

  readonly nickname?: string,
  readonly name?: string,
}

export function issuedAt(claim: AuthnClaim){
  return new Date(claim.iat * 1000);
}

export function expiry(claim: AuthnClaim){
  return new Date(claim.exp * 1000);
}

export type GetTokenFunction = ()=>Promise<string>;
type AuthnContextInfo = {
  isAuthenticated: boolean,
  isLoading: boolean,
  identity: AuthnIdentity,
  claim: AuthnClaim,
  logout: ()=>void,
  getToken: GetTokenFunction,
}

function throwNoProviderError(){
  throw new Error("AuthContext funtion called without setting a provider");
}

