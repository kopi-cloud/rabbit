import Auth0Client from "@auth0/auth0-spa-js/dist/typings/Auth0Client";
import {ErrorDispatch} from "Error/ErrorDialog";
import {Config} from "Config";

const log = console;

export function getAuthnCallbackUrl(){
  return `${window.location.origin}${Config.authnCallbackUrl}`
}

export function isAuthnCallbackUrl(){
  const isAuth0CallbackPath =
    window.location.pathname === Config.authnCallbackUrl;
  const containsAuth0CallbackParam = window.location.search.includes("code=");
  return isAuth0CallbackPath && containsAuth0CallbackParam;
}

export const removeAuth0StateFromUrl = async function(
  auth0FromHook: Auth0Client,
  errorHandler: ErrorDispatch,
){
  if( !isAuthnCallbackUrl() ){
    return;
  }

  // If this component is being mounted as the result of a
  // successful login redirect from Auth0 - notify the Auth0Client to
  // handle the redirect logic, then clear the Auth0 state from the url.
  log.debug("calling handleRedirectCB() ");
  try {
    const {appState} = await auth0FromHook.handleRedirectCallback();

    // Firefox workaround as per:
    // https://github.com/auth0-samples/auth0-react-samples/issues/145
    // noinspection SillyAssignmentJS
    window.location.hash = window.location.hash;  // eslint-disable-line no-self-assign

    window.history.replaceState(
      {},
      document.title,
      appState && appState.targetUrl ?
        appState.targetUrl : window.location.pathname );

    log.debug("handleRedirectCB done");
  } catch( error ){
    log.error("handleRedirectCB failed", error);
    // when this happens using global error handler caused an error loop,
    // workaround is to remove errorHandler from useEffect() dependencies
    errorHandler({
      type: "handleError",
      error: {message: "handleRedirectCB failed", problem: error} });

    // This was an attempted workaround from before I'd read the auth0-spa
    // FAQ (see the comment inside the method).
    // But I still think it makes sense in general though.
    window.history.replaceState({}, document.title, window.location.pathname);
  }
};

