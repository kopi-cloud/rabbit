import {UserInfoV1} from "Server/RabbitApiTypes";
import {useAuthn} from "Auth/AuthenticationProvider";
import {useRabbitApi} from "Auth/ApiProvider";
import * as React from "react";
import {ErrorInfo} from "Error/ErrorUtil";
import {SmallScreenContainer} from "Component/Screen";
import {Button, Typography} from "@material-ui/core";
import {ButtonLabel, primaryButtonProps} from "Component/AppButton";
import {CompactErrorPanel} from "Error/CompactErrorPanel";

export function UnauthorizedUserSignup(props: {
  onRegistered: (user: UserInfoV1) => void
}){
  const authn = useAuthn();
  const api = useRabbitApi();
  const [isRegistering, setIsRegistering] = React.useState(false);
  const [registrationProblem, setRegistrationProblem] = React.useState(
    undefined as undefined | ErrorInfo);

  async function registerRabbitUser(){
    setIsRegistering(true);
    try {
      const result = (await api.registerTrialUser()).result;
      if( typeof result === "string" ){
        setIsRegistering(false);
        setRegistrationProblem({
          message: "while registering user", problem: result
        });
      }
      else {
        // setting these states should be done before the callback (if at all)
        // because the callback is going to unmount the component and we'd
        // be setting state on an unmounted component will cause a warning
        setIsRegistering(false);
        setRegistrationProblem(undefined);
        props.onRegistered(result);
      }
    } catch( error ){
      setIsRegistering(false);
      setRegistrationProblem({
        message: "while registering user", problem: error
      });
    }

  }

  return <SmallScreenContainer center>
    <Typography paragraph>
      {authn.identity.email} is not currently a Rabbit user,
      do you want to "register"?
    </Typography>
    <Button {...primaryButtonProps} onClick={registerRabbitUser}>
      <ButtonLabel isLoading={isRegistering}>
        I <em>♡</em> Rabbits
      </ButtonLabel>
    </Button>
    <br/><br/>&nbsp;
    <CompactErrorPanel error={registrationProblem}/>
  </SmallScreenContainer>
}