import * as React from "react";
import {useContext} from "react";
import {FakeRabbitApi} from "Server/FakeRabbitApi";
import {RabbitApi} from "Server/RabbitApi";
import {GetTokenFunction, useAuthn} from "Auth/AuthenticationProvider";
import {SmallScreenSpinner} from "Component/SmallScreenSpinner";

const log = console;

// even the fake api should fail if used without a provider in scope.
const ApiContext: React.Context<RabbitApi> =
  React.createContext(new FakeRabbitApi(
    undefined as unknown as GetTokenFunction, "fakeApiServer" ) as RabbitApi);

export const useRabbitApi = ()=> useContext(ApiContext);

export function ApiProvider(props: {children: React.ReactNode}){
  const authn = useAuthn();
  const [rabbitApi, setRabbitApi] = React.useState(
    undefined as undefined|RabbitApi );

  React.useEffect(()=>{
    setRabbitApi(new FakeRabbitApi(authn.getToken, "fakeApiServer"));
  }, [authn.getToken]);

  if( !rabbitApi ){
    log.debug("showing spinner because api not configured");
    return <SmallScreenSpinner message={"Configuring API"}/>
  }
  return <ApiContext.Provider value={rabbitApi}>
    {props.children}
  </ApiContext.Provider>
}

