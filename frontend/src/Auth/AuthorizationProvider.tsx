import * as React from "react";
import {useContext} from "react";
import {useRabbitApi} from "Auth/ApiProvider";
import {UserInfoV1} from "Server/RabbitApiTypes";
import {SmallScreenSpinner} from "Component/SmallScreenSpinner";
import {SmallScreenContainer} from "Component/Screen";
import {Typography} from "@material-ui/core";
import {CompactErrorPanel} from "Error/CompactErrorPanel";
import {useAuthn} from "Auth/AuthenticationProvider";
import {LogoutContainer} from "Auth/LogoutContainer";
import {ErrorInfo} from "Error/ErrorUtil";
import {UnauthorizedUserSignup} from "Auth/UnauthorizedUserSignup";

const log = console;

const AuthzContext = React.createContext({
  // it is invalid for app to use this outside of a provider
  email: "no-context-provider-set" } as UserInfoV1 );
export const useAuthz = ()=> useContext(AuthzContext);

export function AuthorizationProvider(props: {children: React.ReactNode}){
  const authn = useAuthn();
  const api = useRabbitApi();
  const [currentUser, setCurrentUser] = React.useState(
    undefined as undefined|UserInfoV1 );
  const [isAuthorizing, setIsAuthorizing] = React.useState(true);
  const [authzProblem, setAuthzProblem] = React.useState(
    undefined as undefined|ErrorInfo);

  React.useEffect(()=>{
    async function findUser(){
      setIsAuthorizing(true);
      try {
        const response = await api.findCurrentUserInfo();
        setCurrentUser(response.currentUser);
      }
      catch( error ){
        setAuthzProblem({
          message: "while finding user info",
          problem: error })
      }
      finally {
        setIsAuthorizing(false)
      }
    }
    findUser();
  }, [api]);


  if( isAuthorizing ){
    log.debug("show spinner because authorizing");
    return <SmallScreenSpinner message={"Authorizing with Rabbit server"}/>
  }

  if( authzProblem ){
    return <>
      <SmallScreenContainer center>
        <Typography paragraph>
          There was a problem while trying to authorize your identity:
          {authn.identity.email}
        </Typography>
        <br/><br/>
        <CompactErrorPanel error={authzProblem} />
      </SmallScreenContainer>
      <LogoutContainer/>
    </>;
  }

  if( !currentUser ){
    // email is not a "registered Rabbit user"
    return <>
      <UnauthorizedUserSignup onRegistered={(user)=>setCurrentUser(user)}/>
      <LogoutContainer/>
    </>;
  }

  return <AuthzContext.Provider value={currentUser}>
    {props.children}
  </AuthzContext.Provider>

}

