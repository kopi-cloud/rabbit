import Auth0Client from "@auth0/auth0-spa-js/dist/typings/Auth0Client";
import React from "react";
import {SmallScreenContainer} from "Component/Screen";
import Typography from "@material-ui/core/Typography";
import {Button} from "@material-ui/core";
import {ButtonLabel, primaryButtonProps} from "Component/AppButton";
import {AuthnIdentity} from "Auth/AuthenticationProvider";

const log = console;

export function UnverifiedEmailContainer(props: {
  auth0Client: Auth0Client,
  identity: AuthnIdentity,
}){
  const [logoutClicked, setLogoutClicked] = React.useState(false);

  return <>
    <SmallScreenContainer center>
      <Typography paragraph>
        The email {props.identity.email} has not been verified with the
        identity provider {mapIdProviderDisplay(props.identity.sub)}.
      </Typography>
      <Typography paragraph>
        You must verify your email address with your identity provider to
        use Rabbit.
      </Typography>
      <Typography paragraph>
        Once you've verified your email address, refresh the browser to
        continue.
      </Typography>
      <br/>
    </SmallScreenContainer>
    <SmallScreenContainer center>
      <Typography paragraph>
        If you'd like to login as a different identity,
        click the logout button.
      </Typography>
      <Button {...primaryButtonProps} onClick={() => {
        log.debug("logout button clicked");
        setLogoutClicked(true);
        props.auth0Client.logout();
      }}>
        <ButtonLabel isLoading={logoutClicked}>
          Logout
        </ButtonLabel>
      </Button>
    </SmallScreenContainer>
  </>;
}

function mapIdProviderDisplay(sub: string){
  if( !sub ){
    return "unknown";
  }

  const split = sub.toLowerCase().split("|");
  if( !split || split.length !== 2 ){
    return "unknown";
  }
  if( !split[0] ){
    return "unknown";
  }
  else if( split[0].startsWith("google") ){
    return "google";
  }
  else {
    return split[0];
  }
}