import {useAuthn} from "Auth/AuthenticationProvider";
import * as React from "react";
import {SmallScreenContainer} from "Component/Screen";
import {Button, Typography} from "@material-ui/core";
import {ButtonLabel, primaryButtonProps} from "Component/AppButton";

const log = console;

export function LogoutContainer(){
  const authn = useAuthn();
  const [logoutClicked, setLogoutClicked] = React.useState(false);

  return <SmallScreenContainer center>
    <Typography paragraph>
      If you'd like to login as a different identity,
      click the logout button.
    </Typography>
    <Button {...primaryButtonProps} onClick={()=>{
      log.debug("logout button clicked");
      setLogoutClicked(true);
      authn.logout();
    }}>
      <ButtonLabel isLoading={logoutClicked}>
        Logout
      </ButtonLabel>
    </Button>

  </SmallScreenContainer>
}