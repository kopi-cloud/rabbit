/** In a fully-fledged app, I'd generate this file based on some API IDL
 * (jsonschema, swagger, etc.) along with metadata or code for validating
 that the data sent by the server matched the current definitions the app
 is using.
 Without validation, the forced casts in RappitApi are a huge foot-gun.
 */
export type UserRoleEnumV1 = "admin" | "user";

export type FeatureEnumV1 = "enable_feature_1" | "enable_feature_2";

export interface UserInfoV1 {
  email: string;
  roles: UserRoleEnumV1[];
  features: FeatureEnumV1[];
}

export interface FindCurrentUserInfoResponseV1{
  currentUser?: UserInfoV1;
}

export interface ApiSvcInfoV1 {
  version: string,
}

export interface RegisterUserResponseV1{
  result: UserInfoV1|string,
}
