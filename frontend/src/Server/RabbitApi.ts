/** Normally, there'd be a bunch of logic in this module about re-tries,
 validating json types, ApiSafe errors, etc.
 Elided in Rabbit for simplicity, so this is fairly over-engineered for
 what it does right now.
 All the code in this class was copied from a working, but more complicated,
 example.  Currently Rabbit is only tested with the FakeRabbitApi because I
 don't want to go through the rigmarole needed to publish an API right now.
 */
import {
  ApiSvcInfoV1,
  FindCurrentUserInfoResponseV1,
  RegisterUserResponseV1
} from "Server/RabbitApiTypes";
import {GetTokenFunction} from "Auth/AuthenticationProvider";
import {delay} from "Util/ObjectUtil";

// const log = console;

export class RabbitApi {

  readonly getToken: GetTokenFunction;
  readonly apiServer:string;

  constructor(getToken:GetTokenFunction, apiServer:string){
    this.getToken = getToken;
    this.apiServer = apiServer;
  }

  async getApiInfo(): Promise<ApiSvcInfoV1> {
    return doUnauthnGet<ApiSvcInfoV1>(this.makeUrl("infoV1"));
  }

  async findCurrentUserInfo(): Promise<FindCurrentUserInfoResponseV1> {
    return this.doAuthnPost<FindCurrentUserInfoResponseV1>(
      this.makeUrl("findCurrentUserInfoV1"),
      {},
    );
  }

  async registerTrialUser(): Promise<RegisterUserResponseV1> {
    return this.doAuthnPost<RegisterUserResponseV1>(
      this.makeUrl("registerUserV1"),
      {},
    );
  }

  async badServerCall(description: string, delayMs: number = 1000)
  : Promise<FindCurrentUserInfoResponseV1> {
    await delay(delayMs);
    throw new Error("intentional error: " + description);
  }

  private makeUrl(endpointSuffix: string):string{
    return `${this.apiServer}/api/${endpointSuffix}`;
  }

  private async doAuthnPost<T>(
    requestUrl: string,
    bodyObject: object,
  ): Promise<T>{
    return doPost(
      requestUrl, bodyObject, await this.getToken())
  }
}

export async function doUnauthnGet<T>(
  requestUrl: string,
): Promise<T>{
  return doGet(requestUrl)
}

async function doGet<T>(
  requestUrl: string,
): Promise<T> {
  // log.debug("get from server", requestUrl);

  let response = await getFetch(requestUrl);

  await checkForServerError(response);

  const json = await response.json();
  // log.debug("result", json);


  // note lack of validation, this cast is unchecked and thus very dangerous
  return json as T;
}

async function getFetch(requestUrl: string, idToken?:string)
  : Promise<Response>
{
  let headers = new Headers();
  appendRabbitHeaders(headers, idToken);
  let response: Response = await fetch(
    requestUrl,
    {
      method: 'GET',
      headers: headers,
    }
  );
  return response;
}

async function doPost<T>(
  requestUrl: string,
  bodyObject: object,
  idToken?: string,
): Promise<T>{
  let body = JSON.stringify(bodyObject);
  // log.debug("post to server", requestUrl, body );

  let json: any;

  // declared as a function so we can call it twice (i.e. retry)
  let jsonServerCall = async () => {
    let response = await postFetch(requestUrl, body, idToken);
    await checkForServerError(response);

    return json;
  };

  json = await jsonServerCall();

  // note lack of validation, this cast is unchecked and thus very dangerous
  return json as T;
}

async function postFetch(
  requestUrl: string,
  body: string = "",
  idToken?: string,
): Promise<Response>
{
  let headers = new Headers();
  appendRabbitHeaders(headers, idToken);
  let response: Response = await fetch(
    requestUrl,
    {
      method: 'POST',
      headers: headers,
      body: body
    }
  );
  return response;
}

function appendRabbitHeaders(headers:Headers, idToken?: string){
  if( idToken ){
    headers.append(
      "Authorization",
      `Bearer ${idToken}`);
  }

  headers.append("Accept", 'application/json');
  headers.append("Content-Type", 'application/json');
  headers.append("Accept-Encoding", 'gzip, deflate');
}

export async function checkForServerError(response: Response) {
  if( response.ok ){
    return;
  }

  // what if it's not json?
  let body = await response.json();

  throw new ServerError(
    "server returned 'not ok'",
    response.status,
    response.statusText,
    true,
    body
  );
}

export class ServerError extends Error {
  public status: number;
  public statusText: string;
  public retryable: boolean;
  public body: object;

  constructor(
    message: string,
    status: number,
    statusText: string,
    retryable: boolean = true,
    body: object)
  {
    super(message);
    this.status = status;
    this.statusText = statusText;
    this.body = body;
    this.retryable = retryable;

    // typescript weirdness, copied from:
    // http://stackoverflow.com/a/41144648/924597
    // Set the prototype explicitly.
    Object.setPrototypeOf(this, ServerError.prototype);
  }

}
