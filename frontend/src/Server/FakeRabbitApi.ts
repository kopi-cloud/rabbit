import {RabbitApi} from "Server/RabbitApi";
import {
  ApiSvcInfoV1,
  UserInfoV1,
  FindCurrentUserInfoResponseV1, RegisterUserResponseV1
} from "Server/RabbitApiTypes";
import {delay} from "Util/ObjectUtil";
import {Config} from "Config";

let jwtDecode = require('jwt-decode');
let log = console;

export class FakeRabbitApi extends RabbitApi {

  async getApiInfo(): Promise<ApiSvcInfoV1>{
    return {version: "fake"};
  }

  // callers of this method must have been autheNticated by Auth0 and must have
  // validated their email, but may not
  // have been authoriZed
  async findCurrentUserInfo(): Promise<FindCurrentUserInfoResponseV1>{
    // all API methods must call getToken(), it's async coz auth0-spa-ja
    // might need to go off and get another access token if the current
    // one has expired
    let token = await this.getToken();

    // After getting the token as above, the api client would then
    // encode it as a bearer token in the metadata of the reqest
    // (e.g. Authorization header for a HTTP request)
    // addHeader("Authorization", "Basic " + token);

    // fake comms delay
    await delay(300);

    // from here down is "fake server logic"

    // this token verification would likely be done in some kind of middleware
    // as a cross-cutting concern across all authenticated endpoints
    const identity = validateIdentity(token);

    // Yup - jave to do some kind of user look up for every single API call.
    // That's the price you pay for separating authn from authz,
    // but now you can just de-authz a user and they can't use the system
    // any more, regardless of token expiry.
    // Except... in order to avoid swamping your datastore with user-queries,
    // you'll do your own caching, but at least you're no longer bound by
    // JWT expiry.
    const user = userDb.findUserByEmail(identity.email);
    return {
      currentUser: user ? user : undefined
    };
  }

  // This endpoint is for admins to call to allow them to change a users
  // role or features
  async addUser(userToAdd: UserInfoV1){
    // client code
    let token = await this.getToken();
    await delay(300);

    // server code

    // AutheNtication: "what is the identity of this user?"
    // Is their bearer token valid and does the source identityProvider
    // vouch for their email?
    const identity = validateIdentity(token);

    // AuthoriZation: "is this user allowed to use our system at all?"
    const currentUser = userDb.findUserByEmail(identity.email);
    if( !currentUser ){
      throw new Error("currentUser is not a DB user: " + identity.email);
    }

    // AuthoriZation: "is this user allowed to perform this action?"
    if( !userDb.isAllowedToAddUser(currentUser) ){
      throw new Error("currentUser cannot add user: " + identity.email)
    }

    userDb.upsertUserByEmail(userToAdd);
  }

  // This endpoint is for users to register themselves, they will be a
  // normal user with no features
  async registerTrialUser():Promise<RegisterUserResponseV1>{
    // const p = new Promise<RegisterUserResponseV1>();
    // client code
    let token = await this.getToken();
    await delay(300);

    // server code

    // AuthN
    const identity = validateIdentity(token);

    if( userDb.findUserByEmail(identity.email) ){
      return Promise.resolve(
        {result: "User already registered with email: " + identity.email} );
    }

    // There's no AuthZ, let trial users register themselves
    const newUser = userDb.addNewUser({
      email: identity.email,
      roles: ["user"],
      features: [],
    });

    return Promise.resolve({result: newUser});
  }
}

/** "sub" is the standard JWT name for "subject" */
const subjectProp = "sub";

function validateIdentity(bearerToken:string): {
  email: string,
  emailVerified: boolean,
  subject: string,
} {
  // A HTTP server would be getting this token from the "Authorization" header
  // of the HTTP request.
  const decodedJwt = jwtDecode(bearerToken);

  // a real server would do a bunch of work here, preferrably
  // using a library written by qualified authors, verifying:
  // - JWT signature by using the public key from the JWKS server
  // - audience, issued-at, epxiry, etc.

  if( !decodedJwt.hasOwnProperty(subjectProp) ){
    throw new Error("invalid access token, no subject claim");
  }
  let jwtSubject:string = decodedJwt[subjectProp];

  // Then, after the we're sure the JWT is legit; we do our own custom
  // checks of the rabbit claims we expect to populated (because we defined
  // Auth0 rules to add them as custom claims on the JWT).
  if( !decodedJwt.hasOwnProperty(Config.rabbitApiEmailProp) ){
    throw new Error("invalid access token, no rabbit email prop");
  }
  let jwtEmail:string = decodedJwt[Config.rabbitApiEmailProp];

  let jwtEmailVerified:boolean = false;
  if( !decodedJwt.hasOwnProperty(Config.rabbitApiEmailVerProp ) ){
    // twitter only send back the email if it's verified
    if( decodedJwt[subjectProp].startsWith("twitter") ){
      jwtEmailVerified = true;
    }
    throw new Error("invalid access token, no rabbit emailVerified prop");
  }
  else {
    jwtEmailVerified = decodedJwt[Config.rabbitApiEmailVerProp];
  }


  // A user must have verified their email address
  if( !jwtEmailVerified ){
    throw new Error("server called with unverified account email: " + jwtEmail);
  }

  return {
    email: jwtEmail,
    emailVerified: jwtEmailVerified,
    subject: jwtSubject,
  }
}

class UserDb {
  users:UserInfoV1[] = [
    { email: "admin-rabbit@kopi.cloud",
      roles: ["admin", "user"],
      features: [] },
    { email: "user1-rabbit@kopi.cloud",
      roles: ["user"],
      features: [] },
  ];

  public findUserByEmail(email: string):undefined|UserInfoV1{
    log.debug("looking for user", email);
    return this.users.find(el=>{
      return el.email === email
    })
  }

  public upsertUserByEmail(user: UserInfoV1){
    this.users = this.users.filter(item=> item.email === user.email);
    this.users.push(user)
  }

  public addNewUser(user: UserInfoV1): UserInfoV1{
    if( !user.email ){
      throw new Error("invalid user object: no email");
    }
    if( !user.roles || user.roles.length < 1 ){
      throw new Error("invalid user object: no roles");
    }

    this.users.push(user);

    return user;
  }

  public isAllowedToAddUser(user:UserInfoV1):boolean{
    return user.roles.includes("admin")
  }
}
const userDb = new UserDb();
