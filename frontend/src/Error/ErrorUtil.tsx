import {TextSpan} from "Component/TextSpan";
import {List, ListItemText} from "@material-ui/core";
import {safeStringify} from "Util/ObjectUtil";
import * as React from "react";
import Button from "@material-ui/core/Button";
import {primaryButtonProps} from "Component/AppButton";

export interface ErrorInfo {
  message: string | JSX.Element;
  problem: any;
}

export function isError<T>(e: T | Error): e is Error{
  if( e === undefined ){
    return false;
  }
  return (e as Error).stack !== undefined;
}

export function isErrorInfo<T>(e: T | ErrorInfo): e is ErrorInfo{
  if( e === undefined ){
    return false;
  }
  return (e as ErrorInfo).message !== undefined &&
    (e as ErrorInfo).problem !== undefined;
}

export function ErrorInfoComponent(props: {
  error: ErrorInfo;
}){
  // explitly calling out that it's "any" to remind that you can't just
  // dump the object into the HTML, need to at least stringify.
  let problem: any = props.error.problem;

  let detailsErrorContent;
  if( problem instanceof Error ){
    console.debug("problem type: Error");
    detailsErrorContent = <span>
      <TextSpan>{problem.name} - {problem.message}</TextSpan>
      <br/>
      <ErrorDetails error={problem}/>
    </span>
  }
  else if( isNonEmptyArrayOfStrings(problem) ){
    console.debug("problem type: string[]");
    // don't need the error screen, there's no further detail to give
    detailsErrorContent = <List>{problem.map((it, index)=>
      <ListItemText key={index}>{it}</ListItemText>
    )}</List>
  }
  else {
    console.debug("problem type: unknown");
    detailsErrorContent = <TextSpan>
      {safeStringify(problem)}
    </TextSpan>
  }

  return <TextSpan>
    <TextSpan>{props.error.message}</TextSpan>
    <br/>
    {detailsErrorContent}
  </TextSpan>;
}

function ErrorDetails(props:{error: Error}){
  const [showMore, setShowMore] = React.useState(false);

  return <div>
    <Button {...primaryButtonProps} onClick={()=> setShowMore(!showMore)}>
      More
    </Button>
    { showMore &&
    <div>
      {formatError(props.error)}
    </div>
    }
  </div>
}

function formatError(err: Error){
  if( !err ){
    return <pre>empty error</pre>;
  }

  if( !err.stack ){
    return <pre>empty error stack</pre>;
  }

  const debugString = safeStringify(err.stack);
  if( !debugString ){
    return <pre>couldn't stringify error</pre>;
  }

  return <pre>{
    debugString.replace(new RegExp(/\\n/, 'g'), "\n")
  }</pre>
}

function isNonEmptyArrayOfStrings(value: any): value is string[] {
  return Array.isArray(value) &&
    !!value.length &&
    value.every(item => typeof item === "string");
}

