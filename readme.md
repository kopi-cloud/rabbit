
See it in action: [https://rabbit.netlify.app](https://rabbit.netlify.app)

### About

Rabbit is an example project, it doesn't actually do anything.

Originally, Rabbit was created as a learning exercise so I could port 
[Kopi](https://kopi.cloud) to the newly released 
[auth0-spa-js](https://github.com/auth0/auth0-spa-js) library.  
Emphasis on the *learning exercise* - maybe don't copy/paste Rabbit 
code to secure your online banking application.

#### Front end

The Rabbit frontend was modelled off of the 
[Auth0 React SPA quick start](https://github.com/auth0-samples/auth0-react-samples/tree/eed6a4b0309f3a71a67c73a6b463085bda9dd361)
as of about July 2019.

Major differences from the quick start are that Rabbit doesn't use 
[react-router](https://github.com/ReactTraining/react-router) and replaces 
[reactstrap](https://reactstrap.github.io/) 
with [Material-UI](https://material-ui.com/) for the component library.  


### Source tree

* [/frontend](frontend) diretory contains the actual rabbit front end project

Future sub-projects:
* an example backend API project 
* an example "infrastructure as code" project


### Frontend architecture

The overall architecture of the frontend app is visible in 
[App.tsx](frontend/src/App.tsx)

It's a "funnel" architecture - tilt your head to the left and it looks a bit 
like a funnel.

The outer edges of the funnel catch edge cases - error, unauthenticated users,
unauthorized users, etc.  As you get closer to the "spout" of the funnel, you
get to the "normal" use-cases of the app (ordinary pages being used by fully
authenticated/authorized users.)

#### Authentication and Authorization 

Rabbit is an example of a slightly more realistic than usual 
authentication and authorization scheme using the 
[OAuth 2.0 PKCE code grant](https://www.google.com/search?client=firefox-b-d&q=OAuth+2.0+PKCE+code+grant+flow) 
flow with [Auth0](https://auth0.com/) and [React](https://reactjs.org/).

Rabbit has a strong separation of authentication from authorization - it uses
Auth0 purely to provide identity services.  Particulary, Rabbit does not use 
the Auth0 generated JWT to carry authorization claims. A Rabbit JWT claims 
only that "Auth0 claims that Google/Facebook/Twitter/Auth0 claims that the 
person who was issued this token controls the email address 'xyz@example.com'".

The token is a chain of trust declaring only that the bearer definitely 
controls the claimed email address.  Whether the user is allowed to use Rabbit
and what they're allowed to do is entirely up to Rabbit to decide.


#### Page routing

Rabbit doesn't use a routing library like `react-router` though it does lift 
a lot of code for the [`useLocation`](frontend/src/Navigation/UseLocation.tsx) 
hook from the [wouter](https://github.com/molefrog/wouter) library.

Rabbit uses a "pull-routing" model. Looking at [App.tsx](frontend/src/App.tsx) - 
all the pages used by authorized use-cases are listed in the "spout".
Each page checks the window location itself to see if it should display.
Linking to pages from the [AppNavBar](frontend/src/Navigation/AppNavBar.tsx) / 
[AppDrawer](frontend/src/Navigation/AppDrawer.tsx) is done by calling a function
exported by each screen component. 

### Future

See the [ToDo](./todo.md) for my list of things I'd like to do with Rabbit in 
the future.


### Local development

* scope out the [ide.md](doc/ide.md) topic for instructions on setting up
your IDE if you use IntelliJ


### Publishing your own Rabbit app

Here's a wiki topic listing the steps to follow to 
[publish your own fork of Rabbit on Netlify](https://kopi-cloud.atlassian.net/wiki/spaces/kopiweb/pages/386040075/Rabbit+-+Publishing+on+Netlify).
It's fairly involved, but pretty straight-forward.
