function twitterIncludeEmail(user, context, callback) {
  // additional request below is specific to Twitter
  if (context.connectionStrategy !== 'twitter') {
    return callback(null, user, context);
  }

  var oauth = require('oauth-sign');
  var request = require('request');
  var uuid = require('uuid');

  var url = 'https://api.twitter.com/1.1/account/verify_credentials.json';
  var consumerKey = 'E4jAWdTBxV8A656mFb26GqXNh';
  var consumerSecretKey = 'XqNFyQq46EJuxS8RYYyydGOngbNgrOpHI9e9rJ0F0afq4414Wt';

  var twitterIdentity = _.find(user.identities, {connection: 'twitter'});
  var oauthToken = twitterIdentity.access_token;
  var oauthTokenSecret = twitterIdentity.access_token_secret;

  var timestamp = Date.now() / 1000;
  var nonce = uuid.v4().replace(/-/g, '');

  var params = {
    include_email: true,
    oauth_consumer_key: consumerKey,
    oauth_nonce: nonce,
    oauth_signature_method: 'HMAC-SHA1',
    oauth_timestamp: timestamp,
    oauth_token: oauthToken,
    oauth_version: '1.0'
  };

  params.oauth_signature = oauth.hmacsign('GET', url, params, consumerSecretKey, oauthTokenSecret);

  var auth = Object.keys(params).sort().map(function (k) {
    return k + '="' + oauth.rfc3986(params[k]) + '"';
  }).join(', ');

  request({
    url: url + '?include_email=true',
    headers: {
      'Authorization': 'OAuth ' + auth
    }
  }, function (err, resp, body) {
    if (resp.statusCode !== 200) {
      return callback(new Error('Error retrieving email from twitter: ' + body || err));
    }

    // this is the line that gets the twitter email into the token
    user.email = JSON.parse(body).email;

    // twitter only sends the email if it's verified
    // NOTE: setting email_verified here doesn't seem to propagate through
    // to the token, not sure why
    user.email_verified = true;
    return callback(err, user, context);
  });
}