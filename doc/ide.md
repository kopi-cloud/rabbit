
Rabbit was authored using Intellij IDEA Ultimate edition.

It's a standard create-react-app, so it should work in whatever IDE you prefer.

Things I configured manually in IDEA:
* Identitify the node.exe file
* mark the src directory as a "Resources Root"
* /Settings/Editor/Code Style/Typescript/Imports tab/
  "Use paths relative to tsconfig.json" = true
* to see NPM tool window, right-lick package.json and "Show NPM Scripts" 