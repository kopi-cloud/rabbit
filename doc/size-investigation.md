
With no changes from the c-r-a intial defaults;  
built via `npm run build`, reports sizes after gzip:

* vendor js chunk: 36.44 KB 
* main js chunk:: 600 B 
* main css chunk: 518 B 

These sizes were confirmed in the Firefox network panel, being served via 
`serve -s build` ("Transferred" column in FF 68.0):

* vendor js chunk: 36.85 KB 
* main js chunk:: 944 B 
* main css chunk: 1.26 KB (vs "Size" column reporting 994 B, whoopsie) 


----

### Add Material-UI

npm install --save @material-ui/core

Without adding any code to actually use Material-UI, build reports same sizes.

----

### Add Material-UI Paper component

In development mode (`npm run start`), the two different import styles report
significantly different gzipped sizes (via FF):

* `import {Paper} from "@material-ui/core";` -> 677 ish KB
* `import Paper from "@material-ui/core/Paper";` -> 450 ish KB 

Production build reported via console output:

* @m-ui/core: 53.66 KB
* @m-ui/core/Paper: 53.66 

Production build reported same size Transferred via FF:

* @m-ui/core: 54.11 KB
* @m-ui/core/Paper: 54.11 KB 

----

### Add Auth0

Add unused auth0 lib, no growth reported.

----

### Implement authentication

This is not just usage of the auth0 lib, but tons of code that actually
uses Material-UI components to display things.  
So we can see from just this that tree shaking appears to be working 
fairly well.

Production build reported via console output:

* 94.11 KB

----
 
### Implement authorization and api calls

Added a bunch more react components, some library updates and probably ended
up using a few new Material-UI components/icons.
We can see the size has stabilised and is only going up by a little, reflecting
new code written and extra components used.

Production build reported via console output:

* 97 KB

----